def "crs_completions" [] {
  ["todo" "schedule" "logs"]
}

export extern "crs" [
    ...targets: string@"crs_completions"
]
