(tool-bar-mode -1) ; Hide the outdated icons
(scroll-bar-mode -1) ; Hide the always-visible scrollbar
(set-fringe-mode 10) ; Give some breathing room
(menu-bar-mode -1) ; Disable the menu bar

(setq inhibit-splash-screen t) ; Remove the "Welcome to GNU Emacs" splash screen
(setq initial-scratch-message nil) ; remove initial message
(defun display-startup-echo-area-message () (message "")) ; no message

(set-charset-priority 'unicode) ; utf-8
(setq
 locale-coding-system 'utf-8
 coding-system-for-read 'utf-8
 coding-system-for-write 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(setq-default fill-column 80) ; ruler at 80 lines 
(set-face-attribute 'fill-column-indicator nil :foreground "grey")
(global-display-fill-column-indicator-mode 1)

(setq use-file-dialog nil) ; Ask for textual confirmation instead of GUI
(defalias 'yes-or-no-p 'y-or-n-p) ; use y/n

(electric-pair-mode 1) ; electric pair mode

(setq backup-directory-alist '(("." . "~/.cache/emacs-backup"))) ; emacs backup
(setq native-comp-async-report-warnings-errors nil) ; warnings off
(setq load-prefer-newer t) ; prefer newer packages
(setq-default indent-tabs-mode nil) ; no tabs, only spaces

(defun i0/enable-line-numbers ()
  "Enable relative line numbers"
  (interactive)
  (display-line-numbers-mode)
  (setq display-line-numbers 'relative))

(add-hook 'prog-mode-hook #'i0/enable-line-numbers t)
(add-hook 'conf-toml-mode-hook #'i0/enable-line-numbers t)
(add-hook 'toml-mode-hook #'i0/enable-line-numbers t)
(add-hook 'toml-ts-mode-hook #'i0/enable-line-numbers t)
(add-hook 'typst-ts-mode-hook #'i0/enable-line-numbers t)

(setq dired-kill-when-opening-new-dired-buffer t)
(setq dired-listing-switches "-alh")

(defvar elpaca-installer-version 0.7)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order
  '(elpaca
    :repo "https://github.com/progfolio/elpaca.git"
    :ref nil
    :files (:defaults (:exclude "extensions"))
    :build (:not elpaca--activate-package)))
(let* ((repo (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list
   'load-path
   (if (file-exists-p build)
       build
     repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28)
      (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop
                   (call-process "git"
                                 nil
                                 buffer
                                 t
                                 "clone"
                                 (plist-get order :repo)
                                 repo)))
                 ((zerop
                   (call-process "git"
                                 nil
                                 buffer
                                 t
                                 "checkout"
                                 (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop
                   (call-process emacs
                                 nil
                                 buffer
                                 nil
                                 "-Q"
                                 "-L"
                                 "."
                                 "--batch"
                                 "--eval"
                                 "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
          (progn
            (message "%s" (buffer-string))
            (kill-buffer buffer))
          (error
           "%s"
           (with-current-buffer buffer
             (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))


; Install use-package support
(elpaca
 elpaca-use-package
 ;; Enable :elpaca use-package keyword.
 (elpaca-use-package-mode)
 ;; Assume :elpaca t unless otherwise specified.
 (setq elpaca-use-package-by-default t))

;; Block until current queue processed.
(elpaca-wait)

(setq custom-file (expand-file-name "customs.el" user-emacs-directory))
(add-hook 'elpaca-after-init-hook (lambda () (load custom-file 'noerror)))

(use-package gcmh :demand :config (gcmh-mode 1))

(use-package
 evil
 :demand
 :init
 (setq
  evil-want-keybinding nil
  evil-respect-visual-line-mode t
  evil-want-minibuffer t
  evil-mode-line-format nil
  evil-undo-system 'undo-redo)
 :config (evil-mode 1) (evil-global-set-key 'normal (kbd "U") 'evil-redo))

(use-package evil-collection :after evil :demand :config (evil-collection-init))

(use-package evil-commentary :config (evil-commentary-mode))

(setq custom-safe-themes t)
(add-to-list 'load-path "~/.config/emacs/themes/")
(load "doom-tokyonight-black-theme")
(enable-theme 'doom-tokyonight-black)

(use-package doom-modeline :ensure t :init (doom-modeline-mode 1))

(use-package nerd-icons :ensure t)

(use-package all-the-icons)
(elpaca-wait)

(use-package all-the-icons-dired :hook (dired-mode . all-the-icons-dired-mode))

(use-package
 dashboard
 :ensure t
 :after (nerd-icons all-the-icons)
 :init
 (setq
  dashboard-icon-type 'all-the-icons
  dashboard-items '((projects . 5) (agenda . 5))
  dashboard-startup-banner "~/.config/emacs/logo.txt"
  dashboard-vertically-center-content t
  dashboard-center-content t
  dashboard-projects-backend 'projectile
  dashboard-set-heading-icons t
  dashboard-set-file-icons t
  initial-buffer-choice (lambda () (get-buffer-create dashboard-buffer-name)))

 (if (daemonp)
     (add-to-list
      'after-make-frame-functions
      (lambda (frame)
        (select-frame frame)
        (dashboard-open))))
 :config (dashboard-setup-startup-hook)
 :hook
 ((elpaca-after-init . #'dashboard-insert-startupify-lists)
  (elpaca-after-init . #'dashboard-initialize)))

(use-package
 centaur-tabs
 :config
 (setq
  centaur-tabs-cycle-scope 'tabs
  centaur-tabs-style "bar"
  centaur-tabs-height 32
  centaur-tabs-set-icons t
  centaur-tabs-set-modified-marker t
  centaur-tabs-set-close-button nil
  centaur-tabs-set-bar 'over)
 (centaur-tabs-mode t)
 :hook
 (dashboard-mode . centaur-tabs-local-mode)
 (calendar-mode . centaur-tabs-local-mode)
 (org-agenda-mode . centaur-tabs-local-mode))

(use-package
 projectile
 :demand
 :init (projectile-mode +1)
 :custom (projectile-enable-caching t))

(use-package
 hl-todo
 :config
 (setq
  hl-todo-highlight-punctuation ":"
  hl-todo-keyword-faces
  '(("TODO" . "#CC3333")
    ("FIXME" . "#FF0000")
    ("DEBUG" . "#A020F0")
    ("GOTCHA" . "#FF4500")
    ("STUB" . "#1E90FF")))
 :hook (prog-mode . hl-todo-mode))

(use-package transient)
(use-package magit :after transient :commands (magit magit-status))
(use-package pinentry :init (pinentry-start))

(use-package
 diff-hl
 :init
 (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
 (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
 :config (global-diff-hl-mode))

(use-package
 vertico
 :init (vertico-mode) (setq vertico-count 10)
 (setq
  evil-complete-next-minibuffer-func 'vertico-next
  evil-complete-previous-minibuffer-func 'vertico-previous))

;; (use-package
;;  corfu
;;  ;; Optional customizations
;;  :custom
;;  ;; (corfu-cycle t)
;;  (corfu-auto t) ;; causes crash
;;  ;; (corfu-auto-prefix 1)
;;  ;; (corfu-auto-delay 0.4)
;;  ;; (corfu-separator ?\s)          ;; Orderless field separator
;;  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
;;  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
;;  (corfu-preview-current nil)
;;  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
;;  (corfu-on-exact-match nil) ;; Configure handling of exact matches
;;  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

;;  :init (global-corfu-mode)
;;  :config (keymap-unset corfu-map "RET"))

(use-package
 company
 :init (global-company-mode)
 :custom (company-minimum-prefix-length 2))

(use-package
 kind-icon
 :after corfu
 :custom
 (kind-icon-default-face 'corfu-default) ; compute blended background correctly
 (kind-icon-blend-background nil)
 :config
 (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter)
 (plist-put kind-icon-default-style :height 0.7))

(use-package helpful)

(use-package
 orderless
 :config
 (setq
  completion-styles '(orderless basic)
  completion-category-defaults nil
  completion-category-overrides '((file (styles . (partial-completion)))))

 (set-face-attribute 'orderless-match-face-0 nil :weight 'normal)
 (set-face-attribute 'orderless-match-face-1 nil :weight 'normal)
 (set-face-attribute 'orderless-match-face-2 nil :weight 'normal)
 (set-face-attribute 'orderless-match-face-3 nil :weight 'normal))

(use-package marginalia :init (marginalia-mode))

(use-package yasnippet :config (yas-global-mode))

(use-package
 treemacs
 :hook (treemacs-mode . (lambda () (display-line-numbers-mode 0)))
 :config
 (treemacs-follow-mode t)
 (treemacs-filewatch-mode t)
 (treemacs-fringe-indicator-mode t))

(use-package treemacs-evil :after (treemacs evil))

(use-package exec-path-from-shell :init (exec-path-from-shell-initialize))

(use-package tree-sitter :config (global-tree-sitter-mode))

(use-package
 tree-sitter-langs
 :hook
 (tree-sitter-after-on . tree-sitter-hl-mode))

(use-package ts-fold :ensure (:type git
:host github
:repo "emacs-tree-sitter/ts-fold")
:after tree-sitter
:hook
(tree-sitter-after-on . global-ts-fold-mode))

;; (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(haskell-mode . haskell-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(sh-mode . bash-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(toml-mode . toml-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(toml-mode . toml-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(conf-toml-mode . toml-ts-mode))

(use-package
 eglot-booster
 :ensure (:host github :repo "jdtsmith/eglot-booster")
 :after eglot
 :hook
 (c++-mode . eglot-ensure)
 (c++-ts-mode . eglot-ensure)
 (c-mode . eglot-ensure)
 (c-ts-mode . eglot-ensure)
 (conf-toml-mode . eglot-ensure)
 (python-mode . eglot-ensure)
 (python-ts-mode . eglot-ensure)
 (haskell-mode . eglot-ensure)
 (haskell-ts-mode . eglot-ensure)
 (typst-ts-mode . eglot-ensure)
 (bash-ts-mode . eglot-ensure)
 (sh-mode . eglot-ensure)
 :init
 (add-to-list
  'eglot-server-programs
  '((toml-ts-mode toml-mode conf-toml-mode) . ("taplo" "lsp" "stdio")))
 (add-to-list
  'eglot-server-programs
  '((sh-mode bash-ts-mode) . ("bash-language-server" "start")))
 :config (eglot-booster-mode))

(setq-default c-basic-offset 4)
(setq-default c-default-style "k&r")
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))

(setq lsp-clients-clangd-args
      '("--header-insertion=never" "--completion-style=detailed"))

(use-package
 rustic
 :init (setq rustic-lsp-client 'eglot)
 :config (setq rustic-format-on-save t)
 :custom
 (rustic-analyzer-command '("rustup" "run" "stable" "rust-analyzer"))
 (rustic-cargo-use-last-stored-arguments t))

(use-package
 typst-ts-mode
 :ensure
 (:type
  git
  :host sourcehut
  :repo "meow_king/typst-ts-mode"
  :files (:defaults "*.el"))
 :config
 (setq-default eglot-workspace-configuration
               '(:exportPdf
                 "onSave"
                 :outputPath "$root/target/$dir/$name"
                 :fontPaths ["./fonts/"]
                 :formatterMode "typstyle")))

(use-package
 rainbow-delimiters
 :config
 (add-hook 'emacs-lisp-mode-hook (lambda () (rainbow-delimiters-mode))))

(use-package
  elisp-autofmt
 :commands (elisp-autofmt-mode elisp-autofmt-buffer)
 :config (setq elisp-autofmt-on-save-p 'always)
 :hook (emacs-lisp-mode . elisp-autofmt-mode))

(use-package
 markdown-mode
 :mode ("README\\.md\\'" . gfm-mode)
 :init (setq markdown-command "multimarkdown")
 :config (setq markdown-fontify-code-blocks-natively t))

(use-package cmake-font-lock)
(use-package glsl-mode)
(use-package haskell-mode)
(use-package nushell-mode)

(defun i0/org-font-setup ()
  "Replace hyphen list with dot"
  (setq evil-auto-indent nil)
  (org-indent-mode)
  (font-lock-add-keywords
   'org-mode
   '(("^ *\\([-]\\) " (0 (prog1 ()
           (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (display-fill-column-indicator-mode 0)
  (set-face-attribute 'org-level-1 nil :height 200)
  (set-face-attribute 'org-level-2 nil :height 180)
  (set-face-attribute 'org-level-3 nil :height 160)
  (set-face-attribute 'org-level-4 nil :height 140))

(defun i0/org-add-ids-to-headlines-in-file ()
  "Add ID properties to all headlines in the current file which
do not already have one."
  (interactive)
  (org-map-entries 'org-id-get-create))

(use-package
 org
 :ensure (:wait t)
 :hook
 ((org-mode . i0/org-font-setup)
  (org-mode
   .
   (lambda ()
     (add-hook 'before-save-hook 'i0/org-add-ids-to-headlines-in-file nil t))))
 :config
 (setq
  org-ellipsis " ▾"
  display-line-numbers nil)
 (setq org-todo-keywords
       '((""
          "TODO(t)"
          "DREAM(D)"
          "ONGOING(o)"
          "NEXT(n)"
          "|"
          "DONE(d!)"
          "CANCELLED(c@)")))
 (setq org-tag-alist
       '(("@home" . ?H)
         ("@acads" . ?W)
         ("@oss" . ?C)
         ("@club" . ?p)
         ("@blogging" . ?w)))
 (setq org-capture-templates
       '(("t"
          "Task"
          entry
          (file "~/Isfaruls_home/org_home/Tasks.org")
          "* TODO %?\n    %U\n    %i"
          :empty-lines 1)
         ("s"
          "Schedule"
          entry
          (file "~/Isfaruls_home/org_home/Schedule.org")
          "* %?\n     %U\n      %i")))


 (setq
  org-preview-latex-default-process 'dvisvgm
  org-latex-create-formula-image-program 'dvisvgm
  org-startup-with-inline-images t
  org-startup-with-latex-preview t)

 (setq
  org-log-done 'time
  org-log-into-drawer t
  org-agenda-start-with-log-mode t
  org-hide-emphasis-markers t)

 (setq org-refile-targets
       '(("~/Isfaruls_home/org_home/Tasks.org" :maxlevel . 1)))
 (advice-add 'org-refile :after 'org-save-all-org-buffers)

 (require 'org-tempo)
 (add-to-list 'org-modules 'org-tempo)
 (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
 (add-to-list 'org-structure-template-alist '("py" . "src python"))
 (add-to-list 'org-structure-template-alist '("hs" . "src haskell"))
 (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
 (add-to-list 'org-structure-template-alist '("cpp" . "src cpp"))

 (org-babel-do-load-languages
  'org-babel-load-languages '((haskell . t) (python . t) (emacs-lisp . t)))
 (setq org-babel-python-command "/usr/bin/python3")
 (setq org-confirm-babel-evaluate nil))

(use-package
 org-bullets
 :after org
 :hook (org-mode . org-bullets-mode)
 :demand t
 :config (setq org-agenda-files '("~/Isfaruls_home/org_home")))

(defun i0/org-mode-visual-fill ()
  (setq
   visual-fill-column-width 100
   visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column :hook (org-mode . i0/org-mode-visual-fill))

(use-package valign :hook (org-mode . valign-mode))

(use-package
 org-journal
 :after org
 :config
 (setq
  org-journal-dir "~/Isfaruls_home/org_home/"
  org-journal-file-type 'yearly
  org-journal-date-format "%A, %d %B %Y"
  org-journal-enable-agenda-integration t))

(use-package simple-httpd :config (httpd-serve-directory "~/Isfaruls_home/age"))

(use-package org-alert
  :after org-bullets
  :config
  (setq alert-default-style 'libnotify
	org-alert-interval 300
	    org-alert-notify-cutoff 10
  )
  (org-alert-enable))

(use-package
 org-node
 :after org)
 ;; (org-node-complete-at-point-mode)
 ;; (setq org-node-extra-id-dirs '("~/org/")))

(use-package
 elfeed
 :config
 (setq elfeed-feeds
       '(("http://blog.knatten.org/feed/" programming)
         ("https://andreasfertig.blog/feed.xml" programming)
         ("https://www.cppstories.com/index.xml" programming)
         ("https://fasterthanli.me/index.xml" programming)
         ("http://nullprogram.com/feed" emacs)
         ("http://pragmaticemacs.com/feed/" emacs)
         ("http://xkcd.com/rss.xml" fun)
         ("https://hackerstations.com/index.xml" fun)
         ("https://innocent_zero.codeberg.page/rss.xml" mine)))
 (setq browse-url-browser-function 'eww-browse-url))

(use-package
 pdf-tools
 :init (pdf-tools-install)
 :config (setq pdf-view-midnight-colors '("#cccccc" . "#0a0a0a")))

(use-package
 which-key
 :demand
 :init
 (setq which-key-idle-delay 0.3) ; Open after .3s instead of 1s
 :config (which-key-mode))

(defun i0/insert-braces ()
  (interactive)
  (if (region-active-p)
      (insert-pair 1 ?{ ?})
    (insert "{}")
    (backward-char)))

(defun i0/insert-square ()
  (interactive)
  (if (region-active-p)
      (insert-pair 1 ?\[ ?\])
    (insert "[]")
    (backward-char)))

(defun i0/insert-quotes ()
  (interactive)
  (if (region-active-p)
      (insert-pair 1 ?\" ?\")
    (insert "\"\"")
    (backward-char)))

(defun i0/insert-single ()
  (interactive)
  (if (region-active-p)
      (insert-pair 1 ?\' ?\')
    (insert "''")
    (backward-char)))

(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer :ignore-auto :noconfirm))

(defun prev-error ()
  "go to previous error"
  (interactive)
  (flymake-goto-prev-error))

(use-package
 general
 :demand
 :config (general-evil-setup)

 (general-create-definer
  leader-keys
  :states '(normal insert visual emacs)
  :keymaps 'override
  :prefix "SPC"
  :global-prefix "C-SPC")

 (leader-keys
  "SPC"
  '(projectile-find-file :which-key "find file")

  "<escape>"
  '(abort-minibuffers :which-key "exit minibuf")

  "b"
  '(:ignore t :which-key "buffer")
  "b <escape>"
  '(keyboard-escape-quit :which-key t)
  "b b"
  '(list-buffers :which-key "list buffers")
  "b d"
  '(kill-current-buffer :which-key "kill buffer")
  "b r"
  '(revert-buffer-no-confirm :which-key "reload buffer")

  "d"
  '(:ignore t :which-key "diagnostics")
  "d <escape>"
  '(keyboard-escape-quit :which-key t)
  "d n"
  '(flymake-goto-next-error :which-key "next error")
  "d p"
  '(prev-error :which-key "previous error")

  "f"
  '(:ignore t :which-key "folding")
  "f a"
  '(ts-fold-close-all :which-key "fold all")
  "f o"
  '(ts-fold-open :which-key "open fold here")
  "f c"
  '(ts-fold-close :which-key "close fold here")

  "g"
  '(:ignore t :which-key "git")
  "g <escape>"
  '(keyboard-escape-quit :which-key t)
  "g g"
  '(magit-status :which-key "status")
  "g l"
  '(magit-log :which-key "log")
  "g n"
  '(diff-hl-next-hunk :which-key "next hunk")
  "g p"
  '(diff-hl-previous-hunk :which-key "previous hunk")

  "h"
  '(:ignore t :which-key "helpful")
  "h <escape>"
  '(keyboard-escape-quit :which-key t)
  "h f"
  '(helpful-function :which-key "search for function")
  "h c"
  '(helpful-callable :which-key "search for callable")
  "h v"
  '(helpful-variable :which-key "search for variable")
  "h s"
  '(helpful-symbol :which-key "search for symbol")
  "h k"
  '(helpful-key :which-key "search for key")

  "l"
  '(:ignore t :which-key "lsp")
  "l <escape>"
  '(keyboard-escape-quit :which-key t)
  "l a"
  '(eglot-code-actions :which-key "code actions")
  "l q"
  '(eglot-shutdown :which-key "shut down lsp")
  "l r"
  '(eglot-rename :which-key "rename symbol")
  "l R"
  '(xref-find-references :which-key "find references")
  "l i"
  '(eglot-find-implementation :which-key "find implementation")
  "l g"
  '(eglot-find-typeDefinition :which-key "find definition")
  "l f"
  '(eglot-format :which-key "format buffer")
  "l d"
  '(eglot-find-declaration :which-key "find declaration")

  "m"
  '(:ignore t :which-key "matching")
  "m <escape>"
  '(keyboard-escape-quit :which-key t)
  "m m"
  '(evil-jump-item :which-key "goto matching")
  "m s"
  '(:ignore t :which-key "add surrounding pair")
  "m s <escape>"
  '(keyboard-escape-quit :which-key t)
  "m s ("
  '(insert-parentheses :which-key "insert parens")
  "m s {"
  '(i0/insert-braces :which-key "insert braces")
  "m s ["
  '(i0/insert-square :which-key "insert square")
  "m s \""
  '(i0/insert-quotes :which-key "insert quote")
  "m s '"
  '(i0/insert-single :which-key "insert quote")

  "o"
  '(:ignore t :which-key "org")
  "o <escape>"
  '(keyboard-escape-quit :which-key t)
  "o a"
  '(org-agenda :which-key "Show agenda")
  "o c"
  '(org-capture :which-key "capture something")
  "o d"
  '(org-deadline :which-key "Add a deadline")
  "o s"
  '(org-schedule :which-key "Add schedule")
  "o t"
  '(org-todo :which-key "Toggle todo")
  "o h"
  '(org-insert-link :which-key "add link")
  "o l"
  '(open-link-at-point :which-key "open link")
  "o j"
  '(org-journal-new-entry :which-key "new journal entry")
  "o e"
  '(org-ctrl-c-ctrl-c :which-key "evaluate region")
  "o x"
  '(org-latex-preview :which-key "toggle Latex")
  "o o"
  '(org-edit-special :which-key "edit block")

  "p"
  '(:ignore t :which-key "projects")
  "p <escape>"
  '(keyboard-escape-quit :which-key t)
  "p p"
  '(projectile-switch-project :which-key "switch project")
  "p a"
  '(projectile-add-known-project :which-key "add project")
  "p r"
  '(projectile-remove-known-project :which-key "remove project")

  "r"
  '(restart-emacs :whick-key "restart emacs")

  "t"
  '(treemacs-add-and-display-current-project-exclusively :which-key "treemacs")

  "w"
  '(:ignore t :which-key "windows")
  "w <escape>"
  '(keyboard-escape-quit :which-key t)
  "w v"
  '(split-window-right :which-key "split vertical")
  "w b"
  '(split-window-below :which-key "split horizontal")
  "w l"
  '(evil-window-right :which-key "goto right window")
  "w h"
  '(evil-window-left :which-key "goto left window")
  "w j"
  '(evil-window-down :which-key "goto below window")
  "w k"
  '(evil-window-up :which-key "goto above window")
  "w q"
  '(delete-window :which-key "delete window")

  "H"
  '(centaur-tabs-backward :which-key "previous tab")
  "L"
  '(centaur-tabs-forward :which-key "next tab")

  "x"
  '(execute-extended-command :which-key "execute command")))

(set-face-attribute 'default nil
                    :font "Fira Code Retina"
                    :height 115
                    :weight 'medium)
(set-face-attribute 'italic nil
                    :font "Source Code Pro Italic"
                    :height 115
                    :slant 'oblique
                    :weight 'medium)
(custom-set-faces
 '(tree-sitter-hl-face:punctuation
   ((t (:inherit 'fixed-pitch))))
 '(tree-sitter-hl-face:embedded ((t (:inherit 'fixed-pitch)))))
