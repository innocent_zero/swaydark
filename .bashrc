append_path()  {
    case ":$PATH:" in
        *:"$1":*) ;;
        *)
            PATH="${PATH:+$PATH:}$1"
            ;;
    esac
}

# Append our default paths
append_path '/home/innocentzero/.local/bin/'
append_path '/home/innocentzero/bin/'
append_path '/home/innocentzero/.tex/bin/x86_64-linux'
append_path '/home/innocentzero/.cargo/bin/'
append_path '/home/innocentzero/.ghcup/bin'
append_path '/home/innocentzero/.cabal/bin'

#force PATH to be visible
export PATH

# unload the path function
unset -f append_path

# important stuff
export TERM=kitty
export TERMINAL=kitty
export EDITOR=helix

# set bat as the highlighter for man
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"

# Qt decorations off
export QT_WAYLAND_FORCE_DPI=physical
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export _JAVA_AWT_WM_NONREPARENTING=1

export GPG_TTY=$(tty)

# init zoxide
eval "$(zoxide init bash)"

# in place of command -h
help() {
    "$@" --help 2>&1 | bathelp
}

lit() {
    git clone --recursive "$1" "/tmp/${1##*/}" && cd "/tmp/${1##*/}" || return
}

function yy() {
    local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
    yazi "$@" --cwd-file="$tmp"
    if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        cd -- "$cwd"
    fi
    rm -f -- "$tmp"
}

. "$HOME/.cargo/env"

[ -f "/home/innocentzero/.ghcup/env" ] && . "/home/innocentzero/.ghcup/env" # ghcup-env


# set env variables for sway to work properly
if [ $(tty) = "/dev/tty1" ]; then
	
    # Make sure there's no already running session.
    if systemctl --user -q is-active niri.service; then
      echo 'A niri session is already running.'
      exit 1
    fi

    # Reset failed state of all user units.
    systemctl --user reset-failed

    # Import the login manager environment.
    systemctl --user import-environment

    # DBus activation environment is independent from systemd. While most of
    # dbus-activated services are already using `SystemdService` directive, some
    # still don't and thus we should set the dbus environment with a separate
    # command.
    if hash dbus-update-activation-environment 2>/dev/null; then
        dbus-update-activation-environment --all
    fi

    # Start niri and wait for it to terminate.
    systemctl --user --wait start niri.service

    # Force stop of graphical-session.target.
    systemctl --user start --job-mode=replace-irreversibly niri-shutdown.target

    # Unset environment that we've set.
    systemctl --user unset-environment WAYLAND_DISPLAY XDG_SESSION_TYPE XDG_CURRENT_DESKTOP NIRI_SOCKET
fi
