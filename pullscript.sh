#!/bin/bash

# start as
# systemctl enable lock@$(id -u)
echo "[System files]"
echo "- Copying lock script"
cp /etc/systemd/system/lock@.service .
echo "- Copying login template"
cp /etc/issue .
echo "- Copying package manager configs"
cp /etc/pacman.conf .
cp /etc/paru.conf .
echo "- Copying sudo customizations"
sudo cp /etc/sudoers.d/insults .

echo "[User files]"
echo "- Copying bash inits"
cp -lf "$HOME/.bashrc" .
cp -lf "$HOME/.bash_profile" .
echo "- Copying gdb inits"
cp -lf "$HOME/.gdbinit" .
cp -lf "$HOME/.gdbearlyinit" .
cp -lf "$HOME/.gdbinit_gef.py" .
echo "- Copying git configs and formatter specs"
cp -lf "$HOME/.gitconfig" .
cp -lf "$HOME/.clang-format" .
cp -lf "$HOME/.rustfmt.toml" .

scripts="./scripts"
dot_config="./dot_config"
userchrome="librewolf_chrome"
discord="vesktop_themes"

mv $scripts "$scripts.bak"
mv $dot_config "$dot_config.bak"
mv $userchrome "$userchrome.bak"
mv $discord "$discord.bak"

echo "[Scripts]"
echo "- Copying bash scripts"
cp -r "$HOME/bin" $scripts
echo "[Configs]"
echo "- Copying config folder"
cp -r "$HOME/.config" $dot_config
echo "- Copying librewolf's userchrome"
cp -r "$HOME/.var/app/io.gitlab.librewolf-community/.librewolf/9c0sneh6.default-default/chrome" $userchrome
echo "- Copying Discord theme"
cp -r "$HOME/.var/app/dev.vencord.Vesktop/config/vesktop/themes" $discord

rm -rf "$scripts/.git"
rm -rf "$dot_config/.git"
rm -rf "$userchrome/.git"
rm -rf "$discord/.git"

rm -rf "$scripts.bak"
rm -rf "$dot_config.bak"
rm -rf "$userchrome.bak"
rm -rf "$discord.bak"
